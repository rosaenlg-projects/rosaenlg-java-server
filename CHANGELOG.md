# Changelog

{::comment}
## [Unreleased]
### Added
### Fixed
### Changed
{:/comment}

## [1.4.0] - 2019-11-07

### Changed

* doc reorganization
* using RosaeNLG 1.4.0


## [1.3.3] - 2019-11-05

### Added

* environment variable `ROSAENLG_HOMEDIR` to set the path of the templates


## [1.3.2] - 2019-11-03

### Added

* initial version



