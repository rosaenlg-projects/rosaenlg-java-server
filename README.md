# RosaeNLG Java Server

Java Server for RosaeNLG, based on [RosaeNLG Java Wrapper](https://gitlab.com/rosaenlg-projects/java-wrapper).


## Documentation

For documentation, see:
- [RosaeNLG documentation](https://rosaenlg.org)
- [JavaDoc](https://www.javadoc.io/doc/org.rosaenlg/java-server/)
- and [here](doc/modules/java-server/java-server.adoc)


## Contrib

(this part of doc is just for me)

- `mvn package -Dmaven.test.skip=true`
- test: `mvn -Dtest=ApplicationTest test`
