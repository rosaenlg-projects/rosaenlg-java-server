package org.rosaenlg.server;

/*-
 * #%L
 * org.rosaenlg:java-server
 * %%
 * Copyright (C) 2019 RosaeNLG.org, Ludan Stoecklé
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.graalvm.polyglot.Value;

import org.json.JSONObject;

import org.rosaenlg.lib.Autotest;
import org.rosaenlg.lib.JsonPackage;
import org.rosaenlg.lib.Wrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RosaeContext {

  private static final Logger logger = LoggerFactory.getLogger(RosaeContext.class);

  private JsonPackage jsonPackage;
  private Wrapper wrapper;
  private Value compiled;
  
  /** Constructor, based on a String containing all the information on a template.
   * 
   * <p>
   * Will create a new GraalVM context for this template, compile the template,
   * test if autotest is activated, and be ready to render multiple times.
   * </p>
   * 
   * @param jsonPackageAsString contains a template and its parameters, JSON format
   * @throws Exception when the JSON package is not well formated, 
   *                   or if the autotest was activated and failed.
   */
  public RosaeContext(String jsonPackageAsString) throws Exception {
    this.jsonPackage = new JsonPackage(jsonPackageAsString);

    logger.info("Constructor RosaeContext, templateId = {}", jsonPackage.getTemplateId());

    this.wrapper = new Wrapper(jsonPackage.getCompileInfo().getLanguage());
    this.compiled = wrapper.compileFromJson(jsonPackage);

    // now can test
    Autotest autotest = jsonPackage.getAutotest();
    if (autotest != null && autotest.getActivate()) {
      logger.info("auto test is activated");

      String rendered = this.render(autotest.getJsonInput());
      for (int i = 0; i < autotest.getExpected().size(); i++) {
        if (!rendered.contains(autotest.getExpected().get(i))) {
          throw new Exception(
            jsonPackage.getTemplateId()
            + " autotest fail on "
            + autotest.getExpected().get(i)
            + " was "
            + rendered);
        }
      }
    }

    logger.info("Constructor RosaeContext done.");
  }

  
  /** Render the template with input data.
   * 
   * @param jsonOptions JSON string containing all the input data to render the template.
   * @return String the rendered result
   * @throws Exception if an error occurs during rendering
   */
  public synchronized String render(String jsonOptions) throws Exception {
    return wrapper.runCompiled(compiled, jsonOptions);
  }

  
  /** Getter on template ID.
   * 
   * @return String the template ID
   */
  public String getTemplateId() {
    return jsonPackage.getTemplateId();
  }

  
  /** Getter on the original JSON package containing the template.
   * 
   * @return String the JSON package containing the template.
   */
  public String getJsonPackageAsString() {
    return jsonPackage.getInitialPackage();
  }

  
  /** Destroys the object by closing its context.
   * 
   * <p>
   * It is recommended to use 'destroy' so that the GraalVM is explicitely closed.
   * </p>
   * 
   * @throws Exception if closing the context failed
   */
  public synchronized void destroy() throws Exception {
    this.wrapper.closeContext();
  }
}
