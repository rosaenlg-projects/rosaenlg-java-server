package org.rosaenlg.server;

/*-
 * #%L
 * org.rosaenlg:java-server
 * %%
 * Copyright (C) 2019 RosaeNLG.org, Ludan Stoecklé
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.apache.commons.io.FileUtils;

import org.json.JSONObject;

import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestRosaeContext {

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(TestRosaeContext.class);

  @Test
  public void createRender() throws Exception {
    
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates-repo/test_inc_no_comp.json"), 
        "utf-8");
    RosaeContext rc = new RosaeContext(jsonPackage);

    assertEquals(rc.getTemplateId(), "test_inc_no_comp");

    String opts = "{ \"language\": \"en_US\" }";
    for (int i = 0; i < 10; i++) {
      String rendered = rc.render(opts);
      logger.debug("rendered: {}", rendered);
      assertEquals(rendered, "<p>Bla included</p>");
    }
  }


  @Test
  public void createRender2Different() throws Exception {
    String[] letters = {"a", "b"};
    for (int i = 0; i < letters.length; i++) {
      String letter = letters[i];

      String jsonPackage = FileUtils.readFileToString(
          new File("test-templates-repo/basic_" + letter + ".json"),
          "utf-8");
      RosaeContext rc = new RosaeContext(jsonPackage);

      assertEquals(rc.getTemplateId(), "basic_" + letter);

      String opts = "{ \"language\": \"en_US\" }";
      for (int j = 0; j < 10; j++) {
        String rendered = rc.render(opts);
        assertTrue(rendered.contains(letter));
      }

    }
  }

  @Test
  public void createRenderLots() throws Exception {

    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates-repo/basic_a.json"),
        "utf-8");

    for (int i = 0; i < 5; i++) {
      // hack the template
      String newJsonPackage = jsonPackage
          .replace("basic_a", "basic_a" + i)
          .replace("aaa", "aaa" + i)
          .replace("Aaa", "Aaa" + i);

      RosaeContext rc = new RosaeContext(newJsonPackage);

      assertEquals(rc.getTemplateId(), "basic_a" + i);

      String opts = "{ \"language\": \"en_US\" }";
      for (int j = 0; j < 10; j++) {
        String rendered = rc.render(opts);
        assertTrue(rendered.contains("Aaa" + i));
      }

    }
  }

  @Test
  public void failAutotest() throws Exception {
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates-repo/basic_a.json"),
        "utf-8");

    // hack the template
    String newJsonPackage = jsonPackage.replace("Aaa", "Xxx");

    assertThrows(Exception.class, () -> {
      new RosaeContext(newJsonPackage);
    });

  }

  @Test
  public void noAutotestAtAll() throws Exception {
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates-repo/basic_a.json"), 
        "utf-8");
    JSONObject parsed = new JSONObject(jsonPackage);
    assertTrue(parsed.has("autotest"));
    parsed.remove("autotest");
    RosaeContext rc = new RosaeContext(parsed.toString());

    String opts = "{ \"language\": \"en_US\" }";
    String rendered = rc.render(opts);
    assertTrue(rendered.contains("Aaa"));
  }

  @Test
  public void autotestHereNotActivated() throws Exception {
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates-repo/basic_a.json"), 
        "utf-8");
    JSONObject parsed = new JSONObject(jsonPackage);
    assertTrue(parsed.has("autotest"));
    assertTrue(parsed.getJSONObject("autotest").getBoolean("activate"));
    parsed.getJSONObject("autotest").remove("activate");
    parsed.getJSONObject("autotest").put("activate", false);

    RosaeContext rc = new RosaeContext(parsed.toString());

    String opts = "{ \"language\": \"en_US\" }";
    String rendered = rc.render(opts);
    assertTrue(rendered.contains("Aaa"));
  }


}
